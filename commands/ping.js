module.exports = {
	name: 'ping',
	description: 'Vérifiez l\'état du bot',
	async execute(interaction){
		await interaction.reply(':white_check_mark: Pong ! :white_check_mark:')
	}
}
