// Warn command
// Include fs module
const fs = require('fs');

module.exports = {
	name: 'warn',
	description: 'Avertir un utilisateur',
	usage: '<utilisateur> <raison>',
	category: 'modération',	
	args: true,
	guildOnly: true,
	options: [
		{
			name: 'utilisateur',
			description: 'L\'utilisateur à avertir',
			required: true,
			type: 'USER'
		},
		{
			name: 'raison',
			description: 'La raison de l\'avertissement',
			required: true,
			type: 'STRING'
		}
	],
	execute(interaction) {
		// Add warn to database in ./warns.json
		let warnDB = JSON.parse(fs.readFileSync('./warns.json', 'utf8'));
		// Check if user is already warned
		// If not, create a new array
		if (!warnDB[interaction.options.get('utilisateur').user.id]) warnDB[interaction.options.get('utilisateur').user.id] = [];
		// Push the warn to the array
		warnDB[interaction.options.get('utilisateur').user.id].push({
			guild: interaction.member.guild,
			raison: interaction.options.get('raison'),
			moderator: interaction.member,
			date: Date.now()
		});
		// Write the new array to the file
		fs.writeFileSync('./warns.json', JSON.stringify(warnDB));
		// Send a embed message
		interaction.reply({
			embeds:[ {
				color: 0xFF0000,
				title: 'Avertissement',
				description: `${interaction.options.get('utilisateur').user} a été averti par ${interaction.member} pour la raison suivante : ${interaction.options.get('raison').value}`
			}]
		});
	}
};

