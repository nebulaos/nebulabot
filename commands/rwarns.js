// Remove warns from a user

const fs = require('fs');

module.exports = {
	name: 'rwarns',
	description: 'Supprime les avertissements d\'un utilisateur.',
	usage: '<utilisateur>',
	args: true,
	guildOnly: true,
	options: [
		{
			name: 'utilisateur',
			description: 'L\'utilisateur dont les avertissements doivent être supprimés.',
			required: true,
			type: 'USER'
		}
	],
	execute(interaction){
		// First load DB of ./warns.json
		let warns = JSON.parse(fs.readFileSync('./warns.json', 'utf8'));
		// Then check if the user has warns
		if(!warns[interaction.options.get('utilisateur').user.id]){
			interaction.reply('Cet utilisateur n\'a pas d\'avertissement.');
			return;
		}
		// If the user has warns, delete them
		delete warns[interaction.options.get('utilisateur').user.id];
		// And save the DB
		fs.writeFileSync('./warns.json', JSON.stringify(warns));
		// And reply
		interaction.reply('Les avertissements de cet utilisateur ont été supprimés.');
	}
};
