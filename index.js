const Discord = require('discord.js')
const fs = require('fs')
require('dotenv').config()

const client = new Discord.Client({intents: [Discord.Intents.FLAGS.GUILDS,'GUILDS','GUILD_MESSAGES']})
client.commands = new Discord.Collection()

let commandFolder = fs.readdirSync(process.cwd() + '/commands').filter(file => file.endsWith('.js'))

for(const file of commandFolder){
	const command = require(process.cwd() + '/commands/' + file)
	client.commands.set(command.name, command) 
}

let eventFolder = fs.readdirSync(process.cwd() + '/events').filter(file => file.endsWith('.js'))

for(const file of eventFolder){
	const event = require(process.cwd() + '/events/' + file)
	if (event.once)
		client.once(event.name, (...args) => {event.execute(client, ...args)})
	else
		client.on(event.name, (...args) => {event.execute(client, ...args)})
}

client.login(process.env.TOKEN)
